package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    
    int maxSize;
    ArrayList<FridgeItem> items;

    //konstruktør 
    public Fridge() {
        maxSize = 20;
        items = new ArrayList<FridgeItem>();
    }

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() < maxSize) {
            items.add(item);
            return true;
        }
        else {
            return false;
        }        
    }

    @Override
    public void takeOut(FridgeItem item) {
        FridgeItem tempitem = null;
        if (!items.isEmpty()) {
            for (FridgeItem itemC : items) {
                if (item.equals(itemC)) {
                    tempitem = item;
                }
            }
            items.remove(tempitem);
        }
        else {
            throw new NoSuchElementException("no such item");
        }
        
    }

    @Override
    public void emptyFridge() {
        items.removeAll(items);        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {        
        ArrayList<FridgeItem> templist = new ArrayList<>();
        for (FridgeItem item : items) {
            if (item.hasExpired()) {
                templist.add(item);
            }
        }
        items.removeAll(templist);
        return templist;
    }


    
}   

